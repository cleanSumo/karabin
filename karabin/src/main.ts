import { createApp } from 'vue'
import './style.css'
import App from './App.vue'

import Header from "./components/Header.vue";
import Summary from "./components/Summary.vue";
import Message from "./components/Message.vue";
import ActionButton from "./components/ActionButton.vue";
import ButtonInfo from "./components/ButtonInfo.vue";
import Information from "./components/Information.vue";
import DataTable from "./components/DataTable.vue";


/* import the fontawesome core */
import { library } from '@fortawesome/fontawesome-svg-core'

/* import font awesome icon component */
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

/* import specific icons */
import { faCircleQuestion, faFilePdf } from '@fortawesome/free-solid-svg-icons'

library.add({faCircleQuestion, faFilePdf});

createApp(App)
.component('font-awesome-icon', FontAwesomeIcon)
.component('Header', Header)
.component('Summary', Summary)
.component('Message', Message)
.component('ActionButton', ActionButton)
.component('ButtonInfo', ButtonInfo)
.component('Information', Information)
.component('DataTable', DataTable)
.mount('#app')
