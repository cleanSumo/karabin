export interface Main {
    quote:            Quote;
    seller:           Seller;
    rows:             Row[];
    textRows:         any[];
    customer:         Customer;
    variantImage:     VariantImage;
    actionActivities: any[];
}

export interface Customer {
    id:                  number;
    customer_number:     number;
    name:                string;
    customer_reference:  string;
    visiting_address:    string;
    delivery_address:    string;
    postal_address:      string;
    org_number:          string;
    phone:               string;
    email:               string;
    website:             string;
    fax:                 string;
    group:               string;
    comment:             string;
    seller_code:         string;
    general_discount:    null;
    special_price_list:  string;
    price_list:          string;
    customer_references: null;
    banned:              boolean;
}

export interface Quote {
    id:                number;
    system_quote_id:   number;
    system_order_id:   null;
    customer_id:       number;
    customer_name:     string;
    customer_number:   string;
    goods_mark:        string;
    vat_amount:        number;
    net_amount:        number;
    total_amount:      number;
    currency_rounding: number;
    comment:           string;
    quote_text:        null;
    status:            string;
    translated_status: string;
    quote_date:        Date;
    meta:              Meta;
    delivery_date:     null;
    remind_at:         null;
    created_at:        Date;
    updated_at:        Date;
}

export interface Meta {
    our_reference:            string;
    customer_reference:       string;
    customer_order_reference: string;
    delivery_name:            string;
    delivery_address:         string;
    delivery_postal_address:  string;
    delivery_terms:           string;
    transport_type:           string;
    payment_terms:            string;
    payment_days:             number;
    total_weight:             number;
    seller_code:              string;
    customer_address:         string;
    customer_postal_address:  string;
    id:                       number;
}

export interface Row {
    id:                  number;
    quote_id:            number;
    system_quote_id:     number;
    system_quote_row_id: number;
    sku:                 string;
    description:         string;
    comment:             null | string;
    unit:                Unit;
    ordered_amount:      number | null;
    delivered_amount:    number | null;
    amount:              number | null;
    discount:            number | null;
    net_amount:          number | null;
    total_amount:        number;
    row_number:          number;
    created_at:          Date;
    updated_at:          Date;
    heading_row:         boolean;
    text_row:            boolean;
}

export enum Unit {
    Empty = "",
    St = "st",
}

export interface Seller {
    id:                         number;
    name:                       string;
    email:                      string;
    phone:                      null;
    first_name:                 string;
    initials:                   string;
    system_username:            string;
    timestamp:                  null;
    roles_array:                any[];
    widgets_array:              any[];
    unread_notifications_count: number;
    seller_image_url:           string;
    seller_image_thumbnail_url: string;
}

export interface VariantImage {
    id:                    number;
    uuid:                  string;
    name:                  string;
    file_name:             string;
    size:                  number;
    disk:                  string;
    conversions_disk:      string;
    collection_name:       string;
    generated_conversions: GeneratedConversions;
    custom_properties:     CustomProperties;
    model_type:            string;
    model_id:              number;
    width:                 number;
    height:                number;
    thumbnailUrl:          string;
    jpgThumbnailUrl:       string;
    originalUrl:           string;
    mediumUrl:             string;
    created_at:            Date;
    updated_at:            Date;
}

export interface CustomProperties {
    width:       number;
    height:      number;
    uploaded_by: string;
}

export interface GeneratedConversions {
    thumb:     boolean;
    medium:    boolean;
    thumb_jpg: boolean;
}
