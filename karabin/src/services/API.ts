

import axios from 'axios';

export default (url='https://karabin.se/api') => {
    return axios.create({
        baseURL: url
    })
}